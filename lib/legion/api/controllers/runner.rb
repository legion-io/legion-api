module Legion
  module API
    module Controllers
      class Runner < Legion::API::Controllers::BaseController
        def model
          Legion::Data::Model::Runner
        end

        get '/:id/extension' do
          Legion::Data::Model::Runner[params['id']].extension.values
        end

        get '/:id/functions' do
          results = {}
          Legion::Data::Model::Function.where(runner_id: params['id']).limit(50).each do |row|
            results[row.values[:id]] = row.values
          end
          results
        end
      end
    end
  end
end
