module Legion
  module API
    module Controllers
      class Extension < Legion::API::Controllers::BaseController
        def model
          Legion::Data::Model::Extension
        end

        def base_path
          '/extension'
        end

        get '/:id/runners' do
          result = {}
          Legion::Data::Model::Extension[params['id']].runners.each do |row|
            result[row.values[:id]] = row.values
          end
          result
        end
      end
    end
  end
end
