module Legion
  module API
    module Controllers
      class Task < Legion::API::Controllers::BaseController
        def model
          @model ||= Legion::Data::Model::Task
        end

        get '/:id/logs' do
          results = []
          item.task_log_dataset.limit(limit).each do |row|
            results.push row.values
          end
          results
        end

        get '/:id/children' do
          results = {}
          model.where(parent_id: params['id']).limit(limit).each do |row|
            results[row.values[:id]] = row.values
          end
          results
        end

        get '/:id/children' do
          model.where(parent_id: params['id']).count
        end

        get '/:id/slaves' do
          results = {}
          model.where(master_id: params['id']).limit(limit).each do |row|
            results[row.values[:id]] = row.values
          end
          results
        end

        get '/:id/slaves/count' do
          model.where(master_id: params['id']).count
        end
      end
    end
  end
end
