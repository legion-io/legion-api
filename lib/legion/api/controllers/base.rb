# frozen_string_literal: true

require 'sinatra/base'
require 'sinatra/contrib'
require 'sinatra/multi_route'
require 'sinatra/respond_with'
require 'sinatra/reloader'

module Legion
  module API
    module Controllers
      class BaseController < ::Sinatra::Base
        register Sinatra::JSON
        register Sinatra::RespondWith

        before do
          content_type :json
        end

        def item
          @item ||= model[params[:id]]
        end

        def limit
          @limit ||= params.key?(:limit) ? params[:limit] : 20
        end

        def offset
          @offset ||= params.key?(:offset) ? params[:offset] : 0
        end

        def order
          @order ||= params.key?(:order) ? params[:order] : 'id'
        end

        after do
          response.body = json(response.body) unless response.body.is_a? String
        end

        get ['', '/'] do
          # results = {}
          # model.limit(limit).offset(offset).each do |row|
          #   results[row.values[:id]] = row.values
          # end
          # results
          model.limit(limit).offset(offset).as_hash(:id, [:id, :status])
          # model.limit(limit).offset(offset).to_hash(:id)

          # model.limit(limit).offset(offset).select_hash_groups(:id, [:id, :status])
          # model.limit(limit).offset(offset).select_hash(:id, [:id, :status])
          # model.limit(limit).offset(offset).select_map([:id, :status])
        end

        post ['', '/'] do
          id = model.insert(params)
          { id: id }
        end

        get '/:id' do
          model[params['id']].values
        end

        delete '/:id' do
        end

        post '/:id' do
        end

        put '/:id' do
        end

        patch '/:id' do
        end
      end
    end
  end
end
