require_relative 'relationship'

module Legion
  module API
    module Controllers
      class Chain < Legion::API::Controllers::BaseController
        def model
          Legion::Data::Model::Chain
        end

        def base_path
          '/chain'
        end

        get '/:id/relationships', provides: [:json] do
          results = {}
          Legion::Data::Model::Relationship.where(chain_id: params['id']).limit(50).each do |row|
            results[row.values[:id]] = row.values
          end
          results
        end

        get '/:id/relationships/:r_id' do
          redirect "/relationship/#{params[:r_id]}"
        end
      end
    end
  end
end
