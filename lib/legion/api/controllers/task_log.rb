module Legion
  module API
    module Controllers
      class TaskLog < Legion::API::Controllers::BaseController
        def model
          Legion::Data::Model::TaskLog
        end
      end
    end
  end
end
