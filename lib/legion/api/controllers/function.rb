module Legion
  module API
    module Controllers
      class Function < Legion::API::Controllers::BaseController
        def model
          Legion::Data::Model::Function
        end

        get '/:id/runner' do
          Legion::Data::Model::Function[params['id']].runner.values
        end

        get ['/:id/runner/extension', '/:id/extension'] do
          Legion::Data::Model::Function[params['id']].runner.extension.values
        end

        get '/:id/as_trigger' do
          results = {}
          Legion::Data::Model::Relationship.where(trigger_id: params['id']).each do |row|
            results[row.values[:id]] = row.values
          end
          results
        end

        get '/:id/as_action' do
          results = {}
          Legion::Data::Model::Relationship.where(action_id: params['id']).each do |row|
            results[row.values[:id]] = row.values
          end
          results
        end

        get '/:id/tasks' do
          results = {}
          Legion::Data::Model::Task.where(function_id: params['id']).limit(50).each do |row|
            results[row.values[:id]] = row.values
          end
          results
        end
      end
    end
  end
end
