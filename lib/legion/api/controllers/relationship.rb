module Legion
  module API
    module Controllers
      class Relationship < Legion::API::Controllers::BaseController
        def model
          Legion::Data::Model::Relationship
        end

        def base_path
          '/relationship'
        end

        get '/:id/chain' do
          Legion::Data::Model::Relationship[params['id']].chain.values
        end

        get '/:id/trigger' do
          Legion::Data::Model::Relationship[params['id']].trigger.values
        end

        get '/:id/action' do
          Legion::Data::Model::Relationship[params['id']].action.values
        end

        get '/:id/tasks' do
          results = {}
          Legion::Data::Model::Task.where(relationship_id: params['id']).limit(50).each do |row|
            results[row.values[:id]] = row.values
          end
          results
        end
      end
    end
  end
end
