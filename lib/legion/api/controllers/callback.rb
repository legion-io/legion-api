module Legion
  module API
    module Callback
      class Trigger < Sinatra::Base
        before do
          content_type :json
        end

        after do
          response.body = Legion::JSON.dump(response.body) unless response.body.is_a? String
        end
        use Rack::LiveReload

        get '/:extension/:runner/:function' do
          extension = Legion::Data::Model::Extension.where(name: params['extension']).first
          runner = extension.runners_dataset.where(name: params['runner']).first
          function = runner.functions_dataset.where(name: params[:function]).first
          routing_key = "#{extension.values[:exchange]}.#{runner.values[:queue]}.#{function.values[:name]}"
          exchange = Legion::Transport::Messages::Dynamic.new(function: function.values[:name], function_id: function.values[:id], routing_key: routing_key)
          args = { message: params['message'], level: params['level'] }
          exchange.options[:args] = args
          task = generate_task_id(function_id: function.values[:id], runner_id: runner.values[:id], args: args)
          exchange.options[:task_id] = task[:task_id]
          exchange.publish
          Legion::JSON.dump(task)
        end

        def generate_task_id(function_id:, status: 'task.queued', **opts)
          insert = { status: status, function_id: function_id }
          insert[:payload] = Legion::JSON.dump(opts[:payload]) if opts.key? :payload
          insert[:function_args] = Legion::JSON.dump(opts[:args]) if opts.key? :args
          %i[master_id parent_id relationship_id task_id].each do |column|
            insert[column] = opts[column] if opts.key? column
          end

          { success: true, task_id: Legion::Data::Model::Task.insert(insert), **insert }
        end
      end
    end
  end
end
