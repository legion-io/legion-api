# frozen_string_literal: true

require 'concurrent-ruby'
require 'rack-livereload'
require 'sinatra/base'
require 'sinatra/namespace'

require_relative 'lib/legion/api'
require_relative 'lib/legion/api/controllers/base'
require_relative 'lib/legion/api/controllers/relationship'
require_relative 'lib/legion/api/controllers/extension'
require_relative 'lib/legion/api/controllers/chain'
require_relative 'lib/legion/api/controllers/function'
require_relative 'lib/legion/api/controllers/runner'
require_relative 'lib/legion/api/controllers/task'
require_relative 'lib/legion/api/controllers/task_log'
require_relative 'lib/legion/api/controllers/trigger'

require 'legion/logging'
Legion::Logging.setup(level: 'info')

require 'legion/settings'
@settings = Legion::Settings.load(config_dir: './settings')

require 'legion/transport'
Legion::Settings.merge_settings('transport', Legion::Transport::Settings.default)
Legion::Transport::Connection.setup

require 'legion/data'
Legion::Settings.merge_settings('data', Legion::Data::Settings.default)
Legion::Data.setup

use Rack::LiveReload
map('/chain') { run Legion::API::Controllers::Chain }
map('/extension') { run Legion::API::Controllers::Extension }
map('/function') { run Legion::API::Controllers::Function }
map('/relationship') { run Legion::API::Controllers::Relationship }
map('/runner') { run Legion::API::Controllers::Runner }
map('/task') { run Legion::API::Controllers::Task }
map('/task_log') { run Legion::API::Controllers::TaskLog }
map('/trigger') { run Legion::API::Controllers::Trigger }
