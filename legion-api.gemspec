# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'legion/api/version'

Gem::Specification.new do |spec|
  spec.name          = 'legion-api'
  spec.version       = Legion::Api::VERSION
  spec.authors       = ['Miverson']
  spec.email         = ['matt.iverson@optum.com']

  spec.summary       = 'The Legion API'
  spec.description   = 'Used to start the Legion API to access data'
  spec.homepage      = 'https://bitbucket.org/legion-io/legion-api'
  spec.license       = 'MIT'

  if spec.respond_to?(:metadata)
    spec.metadata['homepage_uri'] = spec.homepage
    spec.metadata['source_code_uri'] = 'https://bitbucket.org/legion-io/legion-api'
    spec.metadata['changelog_uri'] = 'https://bitbucket.org/legion-io/legion-api/src/CHANGELOG.md'
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      f.match(%r{^(test|spec|features)/})
    end
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'rack-livereload'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-performance'

  spec.add_dependency 'concurrent-ruby'
  spec.add_dependency 'legion-crypt'
  spec.add_dependency 'legion-data'
  spec.add_dependency 'legion-logging'
  spec.add_dependency 'legion-settings'
  spec.add_dependency 'legion-transport'
  spec.add_dependency 'sinatra'
  spec.add_dependency 'sinatra-contrib'
  spec.add_dependency 'rack'
  spec.add_dependency 'puma'
end
